#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cmath>
using namespace std;
double getFunc(double x){
	return x - 0.1;
}
void getX(double leftPoint, double* x, double step, int n){
	//cout << "������ ����� �����: ";
	for (int i = 0; i < n; i++){
		x[i] = leftPoint + i * step; //���������� x[i] �� �������, ��� x[i] - ����� �����
	//	cout << x[i] << " ";
	} //cout << endl;
}
void getConst (double* A, double* B, double* C, double n){
	cout << "������� �������� A " << n - 1 << " ���: "; for (int i = 0; i < n - 1; i++){ cin >> A[i]; }
	cout << "������� �������� B " << n - 1 << " ���: "; for (int i = 0; i < n - 1; i++){ cin >> B[i]; }
	cout << "������� �������� C " << n - 1 << " ���: "; for (int i = 0; i < n - 1; i++){ cin >> C[i]; }
}
void getABCvectors(double* A, double* B, double* C, double* a, double* b, double* c, double* g, double* x, double step, int n){
	for (int i = 0; i < n - 1; i++){
		int j; j = x[i];
		a[i] = A[j];
		b[i] = C[j] * (step * step) - B[j] * step - 2 * A[j];
		c[i] = A[j] + B[j] + B[j] * step;
		g[i] = getFunc(x[i]) * (step * step);
	}
}
void getLM(double* a, double* b, double* c, double* g, double* l, double* m, int n){
	//cout << "�������� ������������ ������� l � m: ";
	for (int i = 1; i < n - 1; i++){
		l[i] = (c[i] * -1) / (a[i] * l[i - 1] + b[i]);
		m[i] = (g[i] - a[i] * m [i - 1]) / (a[i] * l[i - 1] + b[i]);
		//cout << l[i] << ", " << m[i] << "; ";
	} //cout << endl;
}
void goForward(double* l, double* m, double* y, int n){
	//cout << "������� ����� ������ ��������: " << y[n - 1] << " ";
	for (int i = n - 2; i >= 0; i--){
		y[i] = l[i] * y[i + 1] + m[i];
		//cout << y[i] << "; ";
	} //cout << endl;
}
void goReverse(double* l, double* m, double* y, int n){
	//cout << "������� ����� �������� ��������: " << y[n] << " ";
	for (int i = n - 2; i >= 0; i--){
		y[i] = y[i + 1] * l[i] + m[i];
	//	cout << y[i] << "; ";
	} //cout << endl;
}
void printAnswer(double* y, double* l, double* m, int n){
	cout << "����������� ������� l m: ";
	for (int i = 1; i < n - 1; i++){
		cout << l[i] << ", " << m[i] << "; ";
	} cout <<endl << "����� ������� ������, ������� ��������: ";
	for (int i = 0; i < n; i++){
		cout << y[i] << " ";
	} cout << endl;
}
int main(){
	cout << "������� ������ ������� �������� ��� alpha && betta = 0." << endl << "��������: ��������� ��������� 426-3." << endl;
	int n; double leftPoint, rightPoint, step;
	cout << "������� �������� �����  � ������ �������: "; cin >> leftPoint >> rightPoint;
	cout << "������� ���������� �������� �� ����� � �������� ���������: "; cin >> n;
	double* A = new double(n); double* B = new double(n); double* C = new double(n); //������������� �������� - ��������
	double* a = new double(n); double* b = new double(n); double* c = new double(n); double* g = new double(n); //������������� ��������
	double* l = new double(n); double* m = new double(n); //������������� ����������� �������������, ��� ������������ �������
	l[0] = 0; m[0] = 0;
	step = (rightPoint - leftPoint) / n; //������������ ����
	double* x = new double(n); getX(leftPoint, x, n, step); //�������� x[i]
	double* y = new double(n); y[0] = 0; y[n - 1] = 0; //��� ��� alpha && betta = 0, � y[0] && a[n] == 0
	getX(leftPoint, x, step, n); //�������� ����� x[i]
	getConst (A, B, C, n); //�������� ������� ������� ��� ����
	getABCvectors(A, B, C, a, b, c, g, x, step, n); //�������� ������� a, b, c
	getLM(a, b, c, g, l, m, n); //�������� ����������� ������������
	goForward(l, m, y, n); //������ ��������
	goReverse(l, m, y, n); //�������� ��������
	printAnswer (y, l, m, n);
	delete[] A; delete[] B; delete[] C; delete[] a; delete[] b; delete[] c; delete[] g; delete[] x; delete[] y;
	return 0;
}