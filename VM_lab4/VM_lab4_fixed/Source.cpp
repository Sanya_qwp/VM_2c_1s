#include <iostream>
#include <cstdio>
#include <cmath>
using namespace std;

float funcInitForX(float x) { return  x+0.5; } //������� �� �������
float funcInitForY(float y) { return  y*2.1; }
void Lagrange(float *x, float *y, float currentPoint, int pointsAmount) {
	float ourAnswer = 0; //���� ����� ������������ ��� �����
	for (int i = 0; i < pointsAmount; i++) {
		float composition = 1; //���� ����� ������������ ��������� ������ �������������� �������
		for (int j = 0; j < pointsAmount; j++) {
			if (i != j) { composition *= (currentPoint - x[j]) / (x[i] - x[j]); } //���� ������� ��������
		}
		composition *= y[i];
		ourAnswer += composition;
	}
	cout << "������� ��������: " << ourAnswer << endl;
}
void Newton(float *x, float *y, float currentPoint, int pointsAmount) {
	float ourAnswer = y[0]; //���� ����� ������������ ��� �����
	for (int i = 1; i < pointsAmount; i++) {
		float composition = 0; //���� ����� ������������ ��������� ������ �������������� �������
		for (int j = 0; j <= i; j++) {
			float nFactorial = 1; 
			for (int k = 0; k <= i; k++) {
				if (k != j) { nFactorial *= x[j] - x[k]; } //�����������
			}
			composition += y[j] / nFactorial; //y[j] -- �������� ������� � ����� j
		}
		for (int k = 0; k < i; k++) { composition *= currentPoint - x[k]; }
		ourAnswer += composition;
	}
	cout << "������� �������: " << ourAnswer << endl;
}

int main(){
	setlocale(LC_ALL, "Russian");
	int leftEdge, rightEdge, pointsAmount;	float currentPoint = 0;
	/*
		leftEdge � rightEdge -- ����� � ������ ����� ���������;
		pointsAmount -- ���������� �����;
		currentPoint -- �������� �����
	*/
	cout << "������� �������: "; cin >> leftEdge >> rightEdge;
	cout << "������� ���������� �����: "; cin >> pointsAmount;
	cout << "������� �������� �����: "; cin >> currentPoint;
	float *x = new float[pointsAmount], *y = new float[pointsAmount];
	x[0] = leftEdge;	y[0] = funcInitForY(leftEdge);
	cout << "x=" << x[0] << "y=" << y[0] << endl;
	for (int i = 1; i < pointsAmount; i++) {
		x[i] = funcInitForX(x[i-1]);
		y[i] = funcInitForY(x[i]);
		cout << "x=" << x[i] << " y=" << y[i] << endl;
	}
	Lagrange(x, y, currentPoint, pointsAmount);
	Newton(x, y, currentPoint, pointsAmount);
	cout << "������ ��������: " << funcInitForY(currentPoint) << endl;
	system("pause"); return 0;
}