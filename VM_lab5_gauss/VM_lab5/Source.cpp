#include <iostream>
#include <cmath>
#include <cstdlib>
#include <cstdio>
using namespace std;

// ����� ������� ���������
void sysout(double **a, double *y, int n) {
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			cout << a[i][j] << "*x" << j;
			if (j < n - 1) cout << " + ";  
		}
		cout << " = " << y[i] << endl;
	}
	return;
}

//����� ������
double * gauss(double **a, double *y, int n) {
	double *x = new double[n], max;
	int k = 0, index;
	const double eps = 0.001;  // �������� ��������
	while (k < n) {
		// ����� ������ � ������������ a[i][k]
		max = abs(a[k][k]);
		index = k;
		for (int i = k + 1; i < n; i++) {
			if (abs(a[i][k]) > max) {
				max = abs(a[i][k]);
				index = i;
			}
			// ������������ �����
			if (max < eps) { // ��� ��������� ������������ ���������
				cout << "������� ���������� �������� ��-�� �������� ������� " << index <<" ������� �" << endl;
				return 0;
			}
			for (int j = 0; j < n; j++) {
				double temp = a[k][j];
				a[k][j] = a[index][j];
				a[index][j] = temp;
			}
			double temp = y[k];
			y[k] = y[index];
			y[index] = temp;
			// ������������ ���������
			for (int i = k; i < n; i++) {
				double temp = a[i][k];
				if (abs(temp) < eps) continue; // ��� �������� ������������ ����������
				for (int j = 0; j < n; j++) {
					a[i][j] = a[i][j] / temp;
				}
				y[i] = y[i] / temp;
				if (i == k)  continue; // ��������� �� �������� ���� �� ����
				for (int j = 0; j < n; j++) {
					a[i][j] = a[i][j] - a[k][j];
				}
				y[i] = y[i] - y[k];
			}
			k++;
		}
		// �������� �����������
		for (k = n - 1; k >= 0; k--) {
			x[k] = y[k];
			for (int i = 0; i < k; i++) {
				y[i] = y[i] - a[i][k] * x[k];
			}
		}
		return x;
	}
}

/* ������� ������� ������� ��������
double Determinant(int **mas, int m) {
int i, j = 0, d = 0, k = 1, n = m -1;
int **p; p = new int*[m];
for (i = 0; i<m; i++)
p[i] = new int[m];
if (m<1)
cout << "������������ ��������� ����������!" << endl;
if (m == 1) {
d = mas[0][0];
return(d);
}
if (m == 2) {
d = mas[0][0] * mas[1][1] - (mas[1][0] * mas[0][1]);
return(d);
}
if (m>2) {
for (i = 0; i<m; i++) {
//����� ��� ��������� ����� �������(mas, p, i, 0, m);
cout << mas[i][j] << endl;
sysout(p, n, m);
d = d + k * mas[i][0] * Determinant(p, n);
k-= k;
}
}
return(d);
}
*/
int main() {
	double **a, *y, *x;
	int n = 5;
	setlocale(LC_ALL, "Russian");
	//cout << "������� ���������� ���������: ";  cin >> n;
	a = new double*[n];  y = new double[n];
	for (int i = 0; i<n; i++) {
		a[i] = new double[n];
	}
	a[0][0] = 24; a[0][1] = 2; a[0][2] = 1; a[0][3] = 3; a[0][4] = 3;
	a[1][0] = 1; a[1][1] = 48; a[1][2] = 2; a[1][3] = 3; a[1][4] = 1;
	a[2][0] = 2; a[2][1] = 3; a[2][2] = 16; a[2][3] = 1; a[2][4] = 1;
	a[3][0] = 3; a[3][1] = 3; a[3][2] = 2; a[3][3] = 16; a[3][4] = 1;
	a[4][0] = 1; a[4][1] = 1; a[4][2] = 2; a[4][3] = 3; a[4][4] = 32;
	y[0] = 33; y[1] = 103; y[2] = -6; y[3] = 54; y[4] = -22;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			cout << "a[" << i << "][" << j << "]= " << a[i][j] << " ";
			//cin >> a[i][j];
		} cout << endl;
	}
	for (int i = 0; i < n; i++) {
		cout << "y[" << i << "]= " << y[i] << endl;
		//cin >> y[i];
	}
	sysout(a, y, n);
	x = gauss(a, y, n);
	for (int i = 0; i < n; i++)
		cout << "x[" << i << "]=" << x[i] << endl;
	cin.get(); cin.get();
	return 0;
}
