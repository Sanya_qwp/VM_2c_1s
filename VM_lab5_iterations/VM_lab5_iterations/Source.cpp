#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <math.h>
#include <cmath>
#include <cstdio>
using namespace std;
void PrintM(long double **matrix, int n) { //����� �������
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++)
			printf("%10.3Lf", matrix[i][j]);
		cout << endl;
	}
}
void PrintV(long double *vector, int n) { //����� �������
	for (int i = 0; i < n; i++) {
		printf("%10.3Lf", vector[i]);
		cout << endl;
	}
}
void PrintMV(long double **matrix, long double *vector, int n) { //����� ������� � �������
	for (int i = 0; i < n; i++) {
		for (int j = 0; j <= n; j++) {
			if (j == n)
				printf("%10.3Lf", vector[i]);
			else
				printf("%10.3Lf", matrix[i][j]);
		}
		cout << endl;
	}
}
bool CheckConv(long double **A, long double *b, int n) {
	long double sum;
	bool err;
	for (int i = 0; i < n; i++) {
		sum = 0;
		for (int j = 0; j < n; j++) {
			if (j != i)
				sum += abs(A[i][j]);
		}
		if (abs(A[i][i] > sum))
			err = true;
		else
			err = false;
	}
	if (err == true) {
		cout << "\n������ ������� ������������� ������� ����������" << endl << endl;
		return true;
	}
	else {
		cout << "\n������ ������� �� ������������� ������� ����������" << endl << endl;
		return false;
	}
}
void Prov(long double **A, long double *x, long double *p, int n) { //�������� ������������ ������, ������ ���������� b
	for (int i = 0; i < n; i++) {
		long double sum = 0;
		for (int j = 0; j < n; j++) {
			sum += A[i][j] * x[j];
		}
		p[i] = sum;
	}
}
void Iter(int n, long double** A, long double* b, long double e) {
	long double *x = new long double[n];
	for (int i = 0; i < n; i++) {
		x[i] = b[i] / A[i][i];
		//x[i] = 0;
	}
	long double* temp = new long double[n],
						  norm; //���������� �������� 
	int iter = 0;
	do {
		iter++;
		for (int i = 0; i < n; i++) {
			long double sum = 0;
			for (int j = 0; j < n; j++) {
				if (i != j)
					sum += A[i][j] * x[j];
			}
			temp[i] = (b[i] - sum) / A[i][i];
		}
		norm = abs(x[0] - temp[0]);
		for (int k = 0; k < n; k++) {
			if (abs(x[k] - temp[k]) > norm)
				norm = abs(x[k] - temp[k]);
			x[k] = temp[k];
		}
	} while (norm > e);
	cout << "�����: " << endl << endl;
	PrintV(x, n);
	cout << "\n���������� ��������: " << iter << endl << endl;
	long double *p = new long double[n];
	cout << "������� ��������. ������ ���������� ������� ��������� ���������:" << endl << endl;
	Prov(A, x, p, n); PrintV(p, n);
	delete[] temp; delete[] x;
}
void Zeidel(int n, long double** A, long double* b, long double e) {
	long double *x = new long double[n];
	for (int i = 0; i < n; i++) {
		x[i] = b[i] / A[i][i];
	}
	long double *prev = new long double[n],
						 norm; //���������� �������� 
	int iter = 0;
	do {
		iter++;
		for (int i = 0; i < n; i++)
			prev[i] = x[i];
		for (int i = 0; i < n; i++) {
			long double sum = 0;
			for (int j = 0; j < i; j++)
				sum += (A[i][j] * x[j]);
			for (int j = i + 1; j < n; j++)
				sum += (A[i][j] * prev[j]);
			x[i] = (b[i] - sum) / A[i][i];
		}
		norm = abs(x[0] - prev[0]);
		for (int k = 0; k < n; k++) {
			if (abs(x[k] - prev[k]) > norm)
				norm = abs(x[k] - prev[k]);
		}
	} while (norm > e);
	cout << "�����: " << endl << endl;
	PrintV(x, n);
	cout << "\n���������� ��������: " << iter << endl << endl;
	long double *p = new long double[n];
	cout << "������� ��������. ������ ���������� ������� ��������� ���������:" << endl << endl;
	Prov(A, x, p, n); PrintV(p, n);
	delete[] prev; delete[] x;
}
int main() {
	setlocale(LC_ALL, "Russian"); system("cls"); FILE *in = fopen("file_primer.txt", "rt");
	int n; long double e; bool err = false;
	cout << "������� ��������: "; cin >> e;
	fscanf(in, "%d", &n);
	long double **A, **C, *b;
	A = new long double *[n]; C = new long double *[n]; b = new long double[n];
	for (int i = 0; i < n; i++) {
		A[i] = new long double[n];
		C[i] = new long double[n];
	}
	for (int i = 0; i < n; i++) { //������������� ������� � � ������� �������� ���������
		for (int j = 0; j <= n; j++) {
			long double temp;
			fscanf(in, "%Lf", &temp);
			if (j == n)
				b[i] = temp;
			else
				A[i][j] = temp;
		}
	}
	fclose(in);
	cout << "������� A � ������� ��������� ���������:" << endl << endl; PrintMV(A, b, n);
	cout << "\n������� A:" << endl << endl; PrintM(A, n);
	err = CheckConv(A, b, n);
	if (err == true) {
		cout << "����� ������� ��������: " << endl << endl; Iter(n, A, b, e);
		cout << "\n����� �������: " << endl << endl; Zeidel(n, A, b, e);
	}
	for (int i = 0; i < n; i++) { delete[] A[i]; delete[] C[i]; }
	delete[] A; delete[] b; delete[] C;
	cout << endl; system("pause"); return 0;
}