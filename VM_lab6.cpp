#include <iostream>
#include <cmath>
//������ �1 : ������� ������ ���� ���������� ������� ������;
//������ �2 : ������� ������ ���� ������� �����-�����;
double func(double x, double y){
	return cos(x); // ������� ������ �����������
}
void MetodEylera(double a,double b,double h){
	double n=(b-a)/h, x[(int)n], y[(int)n], y1;
	int i;
	x[0]=a; y[0]=0;
	/*for(i=1; i<=n; i++){
            x[i]=a+i*h;
            y[i]=y[i-1]+h*func(x[i-1]+h/2,y[i-1]+h/2*func(x[i-1],y[i-1]));
        }*/
    for (i=1; i<=n; i++){
    	x[i]=x[i-1]+h;
    	y1 = y[i-1]+h*func(x[i-1], y[i-1]);
    	y[i]=y[i-1]+h*(func(x[i-1], y1)+func(x[i], y1))/2;
	}
    //����� ������
	printf(" i   x       y\n");
	for(i=0;i<=n;i++){
		printf("%3d ",i);
		printf(" %4.4f  %4.4f\n",x[i],y[i]);
	}
}


void MetodRungeKutty(double a, double b,double h)
{
	int i;
	double  n=(b-a)/h,x[(int)n],k1,k2,k3,k4,y[(int)n];
	x[0]=a; y[0]=0;
	for(i=1;i<=n;i++){
		x[i]=a+i*h;
		k1=h*func(x[i-1],y[i-1]);
		k2=h*func(x[i-1]+h/2,y[i-1]+k1/2);
		k3=h*func(x[i-1]+h/2,y[i-1]+k2/2);
        k4=h*func(x[i-1]+h,y[i-1]+k3);
        y[i]=y[i-1]+(k1+2*k2+2*k3+k4)/6;
	}
	//����� ������
	printf(" i   x       y\n");
	for(i=0;i<=n;i++){
		cout << i << " " << x[i] << " " y[i] << endl;
	}
	
}
int main(){
	system("chcp 1251");
	//��� ���������� [a,b] � ��� h
	double h=0.1,a,b;
	printf("������� ���������� � � b\n"); scanf("%lf %lf",&a,&b);
	printf("������� ������ ���������� ������� ������\n"); MetodEylera(a,b,h);
	printf("\n������� ������� �����-�����\n"); MetodRungeKutty(a,b,h);
	system("PAUSE"); return 0;
} 

