#define eps 0.00001
#include <iostream>
#include <cstdlib>
#include <cmath>
using namespace std;
double f(double* x, double n){
    double y;
    for (int i = 1; i < n; i++){
        //y = ( x[i] + i ) * i;
        y = ((x[0]-6)*(x[0]-6)) + ((x[1]+7)*(x[1]+7)) + ((x[2]-7)*(x[2]-7));
        y += y;
    }
    return y;
}
double* gradient(double* x, double* pr, double n){
    double* x1 = new double(n); double* x2 = new double(n);
    double dx, f1, f2;
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++){ x1[j] = x[j]; x2[j] = x[j]; }
        f1 = 100; dx = 0.1;
        x1[i] = x1[i] + dx; x2[i] = x2[i] - dx;
        f2 = (f(x1, n) - f(x2, n))/(2*dx);
        while (fabs(f1 - f2)>eps){
            dx/=10; f1 = f2;
            for (int j = 0; j < n; j++){ x1[j] = x[j]; x2[j] = x[j]; }
            x1[i] = x1[i] + dx;
            x2[i] = x2[i] - dx;
            f2 = (f(x1, n) - f(x2, n))/(2*dx);
        }
        pr[i] = f2;
    }
    return pr;
}
void minWasFound(double* x, double* xk, double* grad, double* pr, int n){
    cout << "Точка минимума найдена: ";
    for (int i = 0; i < n; i++){
        cout << "x[" << i << "] = " << xk[i] << "; ";
    } cout << endl;
    delete x; delete xk; delete grad; delete pr;
    exit(1);
}

int main(){
    double alpha = 1, s = 0; cout << "Введите шаг: "; cin >> alpha;
    double n = 1; cout << "Введите количество неизвестных: "; cin >> n;
    double* x = new double(n); double* grad = new double(n);
    double* pr = new double(n); double* xk = new double(n);
    bool flag = false;
    for (int i = 0; i < n; i++){
        cout << "Введите " << i << "-ю начальную координату: ";
        cin >> x[i]; 
    }
    gradient(x, pr, n);
    for (int i = 0; i < n; i++){
        grad[i] = pr[i];
        s+= (grad[i] * grad[i]);
    }
    if (s<eps){ minWasFound(x, xk, grad, pr, n); }
    while(1){
        int j = 0; s = 0;
        for (int i = 0; i < n; i++){
            xk[i] = x[i] - (alpha*fabs(grad[i]));
        }
        if (f(x,n) < f(xk,n)){ alpha/=2; }
        //else { minWasFound(x, xk, grad, pr, n); }
        for (int i = 0; i < n; i++){
            s+=grad[i]*grad[i];
        }
        for (int i = 0; i < n; i++){
            if ((xk[i] - x[i]) < eps){
                flag = true; break;
            }
        }
        if ((s < eps) || (flag)){ minWasFound(x, xk, grad, pr, n); }
        for (int i = 0; i < n; i++){
            x[i] = xk[i];
        }
        gradient(xk, pr, n);
        for (int i = 0; i < n; i++){
            grad[i] = pr[i];
        }
        j++;
    }
}